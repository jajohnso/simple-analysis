#!/bin/bash
set -e
TEMPFILE=`mktemp` && (
  find SimpleAnalysisCodes/src -name "ANA-*" | xargs -I {} bash -c 'echo {} $(sed -n "s/.*DefineAnalysis(\(.*\?\)).*/\1/p" {})' | awk '{split($1, a, "/"); ANALYSIS=substr(a[3],5,length(a[3])-8); split(ANALYSIS, GLANCE, "_"); printf "| %-148s | %-86s |\n", "["$2"](https://gitlab.cern.ch/atlas-sa/simple-analysis/-/blob/master/"$1")", "[ANA-"GLANCE[1]"](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/"GLANCE[1]")"}' >> $TEMPFILE
  sed -e '/<!-- DYNAMIC CONTENT HERE -->/ {' -e 'r '$TEMPFILE -e 'd' -e '}' -i docs/src/analyses.md
  rm $TEMPFILE
)
