---
title: Analysis implementation
---

Sooner or later, you will find yourself trying to debug non-functioning code. SimpleAnalysis offers two convenience methods that help you debugging.

### PrintObjects

The AnalysisClass implements the following two methods

```cpp
void printObject(const AnalysisObject& obj);
void printObjects(const AnalysisObjects& objs);
```
that can be used to print out a single analysis object or a list of analysis objects. E.g.

```cpp
printObjects(largeRJets);
```
using the previously created list of `largeRJets`, prints the following for a random event:
```sh
0 Pt: 79.0915 Phi: 2.52369 Eta:0.256363 charge: 0 id: 0 type:7
1 Pt: 72.3564 Phi: -2.6827 Eta:-0.675172 charge: 0 id: 0 type:7
2 Pt: 45.0344 Phi: 0.846526 Eta:2.04546 charge: 0 id: 0 type:7
3 Pt: 36.3744 Phi: -0.699859 Eta:-1.6041 charge: 0 id: 0 type:7
```

### DebugObjects

In addition to the `printObject` methods, the following two macros exist:
```cpp
#define DebugObject(obj,num)
  do {
    static int cnt=0;
    if (num || cnt<num) printObject(obj);
    cnt++;
  } while(0)

#define DebugObjects(objs,num)
  do {
    static int cnt=0;
    if (num || cnt<num) printObjects(objs);
    cnt++;
  } while(0)
```

These two macros also allow you to get print out messages of your analysis objects using
```cpp
DebugObjects(largeRJets,1);
```
