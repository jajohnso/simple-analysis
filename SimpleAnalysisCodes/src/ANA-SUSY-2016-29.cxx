#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(StopBL2016)

void StopBL2016::Init()
{
  addRegions({"SR800","SR1100"});
}

void StopBL2016::ProcessEvent(AnalysisEvent *event)
{
  auto baseElectrons  = event->getElectrons(10, 2.47, EVeryLooseLH);
  auto baseMuons      = event->getMuons(10,2.7, MuLoose);
  auto baseJets       = event->getJets(20., 2.8); 
  //auto baseTaus       = event->getTaus(20., 2.5, TauLoose); 
  //auto metVec         = event->getMET();
  //float Met = metVec.Pt();
  
  // Reject events with bad jets
  // CHECK: right place?
  //if (countObjects(baseJets, 20, 4.5, NOT(LooseBadJet))!=0) return;

  // overlap removal
  auto radiusCalcLepton = [] (const AnalysisObject& lepton, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/lepton.Pt()); };
  auto muJetSpecial = [] (const AnalysisObject& jet, const AnalysisObject& muon) { 
    if (jet.pass(NOT(BTag77MV2c20)) && (jet.pass(LessThan3Tracks) || muon.Pt()/jet.Pt()>0.7)) return 0.2;
    else return 0.;
  };
  baseMuons     = overlapRemoval(baseMuons, baseElectrons, 0.01, NOT(MuCaloTaggedOnly));
  baseElectrons = overlapRemoval(baseElectrons, baseMuons, 0.01);

  baseJets      = overlapRemoval(baseJets, baseElectrons, 0.2, NOT(BTag77MV2c20));
  baseElectrons = overlapRemoval(baseElectrons, baseJets, 0.2);

  baseJets      = overlapRemoval(baseJets, baseMuons, muJetSpecial, NOT(BTag77MV2c20));
  baseMuons     = overlapRemoval(baseMuons,baseJets, 0.2);

  baseMuons     = overlapRemoval(baseMuons,baseJets, radiusCalcLepton);
  baseElectrons = overlapRemoval(baseElectrons,baseJets, radiusCalcLepton);
  
  //baseTaus      = overlapRemoval(baseTaus, baseElectrons, 0.1);

  auto baseLeptons     = baseElectrons+baseMuons;
  auto signalJets      = filterObjects(baseJets, 60, 2.5);
  //auto signalBJets     = filterObjects(signalJets, 60, 2.5, BTag77MV2c20);
  //auto nonBJets        = filterObjects(signalJets, 60, 2.5, NOT(BTag77MV2c20));
  auto signalElectrons = filterObjects(baseElectrons, 40, 2.47, ELooseLH|ED0Sigma5|EZ05mm|EIsoLooseTrack);
  auto signalMuons     = filterObjects(baseMuons, 40, 2.7, MuD0Sigma3|MuZ05mm|MuIsoLooseTrack);
  auto signalLeptons   = signalElectrons+signalMuons;
  
  if (signalLeptons.size()< 2 || signalJets.size()<2)  return;

  auto lep0 = signalLeptons[0];
  auto lep1 = signalLeptons[1];
  auto jet0 = signalJets[0];
  auto jet1 = signalJets[1];
  int leadb1j1=0;
  int leadb1j2=0;
  if (jet0.pass(TrueBJet)) leadb1j1=1;
  if (jet1.pass(TrueBJet)) leadb1j2=1;
  int isOS=1;
  if(lep0.charge()==lep1.charge()) isOS=0;
  double mll=(lep0 + lep1).M();

  //accept("CiaoKerim");
  //std::cout << "blablabla. leadb1j1: " << leadb1j1 << " isOS: " << isOS << " mll: " << mll << std::endl;

  double mb0l0=(jet0+lep0).M();
  double mb0l1=(jet0+lep1).M();
  double mb1l0=(jet1+lep0).M();
  double mb1l1=(jet1+lep1).M();

  double diff1=abs(mb0l0-mb1l1);
  double sum1=mb0l0+mb1l1;
  double diff2=abs(mb0l1-mb1l0);
  double sum2=mb0l1+mb1l0;

  double asym1=(diff1/sum1);
  double asym2=(diff2/sum2);
  double mBLacc1=0.;
  //double mBLacc2=0.;
  //double mBLrej1=0.;
  double mBLrej2=0.;
  double mBLasym=0.;

  if (asym1<asym2)
    {
      mBLasym=asym1;
      if (mb1l1>mb0l0)
	{
	  mBLacc1=mb1l1;
	  //mBLacc2=mb0l0;
	}
      else
	{
	  mBLacc1=mb0l0;
	  //mBLacc2=mb1l1;
	}
      if (mb1l0>mb0l1)
	{
	  //mBLrej1=mb1l0;
	  mBLrej2=mb0l1;
	}
      else
	{
	  //mBLrej1=mb0l1;
	  mBLrej2=mb1l0;
	}
    }
  else
    {
      mBLasym=asym2;
      if (mb1l0>mb0l1)
	{
	  mBLacc1=mb1l0;
	  //mBLacc2=mb0l1;
	}
      else
	{
	  mBLacc1=mb0l1;
	  //mBLacc2=mb1l0;
	}
      if (mb1l1>mb0l0)
	{
	  //mBLrej1=mb1l1;
	  mBLrej2=mb0l0;
	}
      else
	{
	  //mBLrej1=mb0l0;
	  mBLrej2=mb1l1;
	}
    }


  if (signalLeptons.size()>1 && signalJets.size()>1 && (leadb1j1||leadb1j2) && isOS && mBLasym<0.2 && mll>300 && mBLacc1>800 && mBLrej2>150) accept("SR800");
  if (signalLeptons.size()>1 && signalJets.size()>1 && (leadb1j1||leadb1j2) && isOS && mBLasym<0.2 && mll>300 && mBLacc1>1100 && mBLrej2>150) accept("SR1100");

  return;
}
