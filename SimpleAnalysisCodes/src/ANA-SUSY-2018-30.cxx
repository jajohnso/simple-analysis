#include "SimpleAnalysisFramework/AnalysisClass.h"

#include <iostream>
#include <iterator>
#include <fstream>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

DefineAnalysis(ThreeBjets_NN_2020)

template <typename T>
std::vector<T> as_vector(boost::property_tree::ptree const& pt, boost::property_tree::ptree::key_type const& key)
{
    std::vector<T> r;
    for (auto& item : pt.get_child(key))
        r.push_back(item.second.get_value<T>());
    return r;
}

struct Parameter {
  int isGtt;
  int mGluino;
  int mLSP;

  std::string stringify() const {
    std::string output("NNoutput_");
    return output + std::to_string(isGtt) + "_" + std::to_string(mGluino) + "_" + std::to_string(mLSP);
  };

};

struct Config {
  std::pair<std::vector<float>, std::vector<float>> normalization;
  std::vector<Parameter> parameters;
};

static struct Config NN_config;

void ThreeBjets_NN_2020::Init()
{
  addRegions({
    "SR_Gbb_B",
    "SR_Gbb_M",
    "SR_Gbb_C",
    "SR_Gtb_B",
    "SR_Gtb_M",
    "SR_Gtb_C",
    "SR_Gtt_0L_B",
    "SR_Gtt_0L_M1",
    "SR_Gtt_0L_M2",
    "SR_Gtt_0L_C",
    "SR_Gtt_1L_B",
    "SR_Gtt_1L_M1",
    "SR_Gtt_1L_M2",
	"SR_Gtt_1L_C",
	"SR_Gbb_2000_1800",
	"SR_Gbb_2800_1400",
	"SR_Gbb_2300_1000",
	"SR_Gbb_2100_1600",
	"SR_Gtt_1900_1400",
	"SR_Gtt_2300_1200",
	"SR_Gtt_1800_1",
	"SR_Gtt_2100_1"
  });

  addRegions({
    "CR_Gbb_B",
    "CR_Gbb_M",
    "CR_Gbb_C",
    "CR_Gtb_B",
    "CR_Gtb_M",
    "CR_Gtb_C",
    "CR_Gtt_0L_B",
    "CR_Gtt_0L_M1",
    "CR_Gtt_0L_M2",
    "CR_Gtt_0L_C",
    "CR_Gtt_1L_B",
    "CR_Gtt_1L_M1",
    "CR_Gtt_1L_M2",
    "CR_Gtt_1L_C"
  });

  addRegions({
  "VR_Gbb_B",
  "VR_Gbb_M",
  "VR_Gbb_C",
  "VR_Gtb_B",
  "VR_Gtb_M",
  "VR_Gtb_C",
  "VR_Gtt_0L_B",
  "VR_Gtt_0L_M1",
  "VR_Gtt_0L_M2",
  "VR_Gtt_0L_C",
  "VR1_Gtt_1L_B",
  "VR1_Gtt_1L_M1",
  "VR1_Gtt_1L_M2",
  "VR1_Gtt_1L_C",
  "VR2_Gtt_1L_B",
  "VR2_Gtt_1L_M1",
  "VR2_Gtt_1L_M2",
  "VR2_Gtt_1L_C",
  });

  addHistogram("meffi",20,0,2000);
  addHistogram("met",20,0,2000);
  addHistogram("mjsum",20,0,2000);
  addHistogram("mTb_min",20,0,800);
  addHistogram("mT", 20, 0, 500);
  addHistogram("dphiMin4", 50, 0, 5);
  addHistogram("dphi1jet", 50, 0, 5);

  addHistogram("n_jets", 20, -0.5, 19.5);
  addHistogram("n_bjets",20, -0.5, 19.5);
  addHistogram("n_electrons", 20, -0.5, 19.5);
  addHistogram("n_muons", 20, -0.5, 19.5);
  addHistogram("n_leptons", 20, -0.5, 19.5);

  addHistogram("GenMET",40,0,2000);
  addHistogram("GenHT",40,0,2000);

  addHistogram("mc_weight", 1, 0, 1);

#ifdef ROOTCORE_PACKAGE_BTaggingTruthTagging
#pragma message "Compiling BTaggingTruthTagging for TRF usage"
  m_btt = new BTaggingTruthTaggingTool("MyBTaggingTruthTaggingTool");
  StatusCode code = m_btt->setProperty("TaggerName",          "MV2c10");
  if (code != StatusCode::SUCCESS) throw std::runtime_error("error setting BTaggingTruthTaggingTool TaggerName property");
  code = m_btt->setProperty("OperatingPoint", "FixedCutBEff_77");
  if (code != StatusCode::SUCCESS) throw std::runtime_error("error setting BTaggingTruthTaggingTool OperatingPoint property");
  code = m_btt->setProperty("JetAuthor", "AntiKt4EMTopoJets");
  if (code != StatusCode::SUCCESS) throw std::runtime_error("error setting BTaggingTruthTaggingTool JetAuthor property");
  //code = m_btt->setProperty("ScaleFactorFileName", "xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-2017-01-31_v1.root");
  code = m_btt->setProperty("ScaleFactorFileName", "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-02-09_v1.root");
  if (code != StatusCode::SUCCESS) throw std::runtime_error("error setting BTaggingTruthTaggingTool ScaleFactorFileName property");
  code = m_btt->setProperty("MaxNtagged", 4);
  if (code != StatusCode::SUCCESS) throw std::runtime_error("error setting BTaggingTruthTaggingTool MaxNtagged property");
  // call initialize() function
  code = m_btt->initialize();
  if (code != StatusCode::SUCCESS) throw std::runtime_error("error initializing the BTaggingTruthTaggingTool");
  std::cout << "Initialized BTaggingTruthTagging tool" << std::endl;
#endif

  // load in config/constants for the neural network
  std::string config_file_path(FindFile("ANA-SUSY-2018-30_config.json"));
  std::ifstream config_file(config_file_path);

  boost::property_tree::ptree pt;
  boost::property_tree::read_json(config_file, pt);

  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);

  NN_config.normalization.first = as_vector<float>(pt, "normalization.mean");
  NN_config.normalization.second = as_vector<float>(pt, "normalization.stddev");

  for(auto& item : pt.get_child("parameters")){
    Parameter p = {item.second.get<int>("isGtt"), item.second.get<int>("mGluino"), item.second.get<int>("mLSP")};
    NN_config.parameters.push_back(p);
  }

  // order of inputs is in the config file
  addONNX("model","ANA-SUSY-2018-30_model.onnx");
}

static int jetFlavor(AnalysisObject &jet) {
  if (jet.pass(TrueBJet)) return 5;
  if (jet.pass(TrueCJet)) return 4;
  if (jet.pass(TrueTau)) return 15;
  return 0;
}

void ThreeBjets_NN_2020::ProcessEvent(AnalysisEvent *event)
{

  float gen_met      = event->getGenMET();
  float gen_ht       = event->getGenHT();
  int channel_number = event->getMCNumber();
  std::vector<float> nn_inputs(87);

  fill("mc_weight", event->getMCWeights()[0]);

  // handle HT slicing now
  if(channel_number==410000 && gen_ht>600) return;
  if(channel_number==410001 && gen_ht>600) return;
  if(channel_number==410002 && gen_ht>600) return;
  if(channel_number==410225 && gen_ht>600) return;
  if(channel_number==410004 && gen_ht>600) return;
  if(channel_number==407018 && gen_ht>500) return;
  if(channel_number==407020 && gen_ht>500) return;

  // // baseline electrons are requested to pass the loose likelihood identification criteria
  // //    and have pT > 20 GeV and |eta| < 2.47
  // auto electrons  = event->getElectrons(20, 2.47, ELooseLH);
  // // baseline muons are required to pass the Medium selections and to have pT > 20 GeV, |eta| < 2.5
  // auto muons      = event->getMuons(20, 2.5, MuMedium);
  // // small-R jets: pT > 20 GeV, |eta| < 2.8
  // auto candJets   = event->getJets(20., 2.8);

  // 1. Get baseline objects
  auto base_electrons = event->getElectrons(20.0, 2.47, ELooseBLLH);
  auto base_muons     = event->getMuons(20.0, 2.5, MuMedium);
  auto base_jets      = event->getJets(30.0, 2.8, JVTMedium);
  if (base_jets.size()==0) return;

  auto metVec     = event->getMET();
  double met      = metVec.Et();
  double met_phi      = metVec.Phi();

  // if(countObjects(base_jets, 20, 2.8, NOT(LooseBadJet))!=0) return;
  // No bad muon veto implemented

  // candJets = filterObjects(candJets, 20, 2.8, JVT50Jet);
  // // reclusterJets(jets, ReclusterRadius, RCJetPtMin, RCJetSubjetRadius, RCJetPtFrac)
  // auto fatJets = reclusterJets(candJets, 0.8, 100, 0.2, 0.10);

  // // Overlap removal
  // auto radiusCalcJet  = [] (const AnalysisObject& , const AnalysisObject& muon) { return std::min(0.4, 0.04 + 10/muon.Pt()); };
  // auto radiusCalcMuon = [] (const AnalysisObject& muon, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/muon.Pt()); };
  // auto radiusCalcElec = [] (const AnalysisObject& elec, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/elec.Pt()); };

  // electrons  = overlapRemoval(electrons, muons, 0.01);
  // candJets   = overlapRemoval(candJets, electrons, 0.2, NOT(BTag77MV2c10));
  // electrons  = overlapRemoval(electrons, candJets, radiusCalcElec);
  // candJets   = overlapRemoval(candJets, muons, radiusCalcJet, LessThan3Tracks);
  // muons      = overlapRemoval(muons, candJets, radiusCalcMuon);

  // Defin a a cone that shrinks with increasing pT
  auto radiusCalcLep = [] (const AnalysisObject& lep, const AnalysisObject&) {
    return (0.04 + 10/lep.Pt()) > 0.4 ? 0.4 : 0.04 + 10/lep.Pt();
  };

  // Remove electrons that share a track with a muon candidate
  base_electrons = overlapRemoval(base_electrons, base_muons, 0.01);

  // Lep-jet overlap removal (refer to the note for the phusics details and numbers)
  base_jets      = overlapRemoval(base_jets, base_electrons, 0.2);
  base_electrons = overlapRemoval(base_electrons, base_jets, radiusCalcLep);

  // Mu-jet overlap removal (refer to the note for the phusics details and numbers)
  base_jets  = overlapRemoval(base_jets, base_muons, 0.2, LessThan3Tracks);
  base_muons = overlapRemoval(base_muons, base_jets, radiusCalcLep);

  auto base_leptons   = base_electrons + base_muons;

  // No cosmic muon veto implemented

  // 5. Signal obejts definition
  auto signal_electrons = filterObjects(base_electrons, 20.0, 2.47, ED0Sigma5 | EZ05mm | EMediumLH);
  auto signal_muons     = filterObjects(base_muons, 20.0, 2.5, MuD0Sigma3 | MuZ05mm | MuMedium | MuIsoFCTightTrackOnly);
  auto signal_leptons   = signal_electrons + signal_muons; // looked it up; seems like the sum is sorted wrt pT
  auto signal_jets      = filterObjects(base_jets, 30.0, 2.8, JVT120Jet);
  auto largeR_jets      = reclusterJets(signal_jets, 0.8, 100.0, 0.2, 0.10); // used rclus=0.2 from an example, couldn't find the value neither in the int note 139fb-1 (draft) nor in 79fb-1
  auto signal_bjets     = filterObjects(base_jets, 30.0, 2.5, BTag77MV2c20); // eta 2.5 or 2.8?

  // // require signal jets to be 30 GeV
  // auto signalJets      = filterObjects(candJets, 30);
  // // signal electrons are required to pass the Medium likelihood criteria and isolated using LooseTrackOnly
  // auto signalElectrons = filterObjects(electrons, 20, 2.47, ETightLH|ED0Sigma5|EZ05mm|EIsoBoosted);
  // // signal muons are required to be isolated using LooseTrackOnly
  // auto signalMuons     = filterObjects(muons, 20, 2.5, MuD0Sigma3|MuZ05mm|MuIsoBoosted|MuNotCosmic);
  // // combine into signalLeptons for easy counting
  // auto signalLeptons   = signalElectrons + signalMuons;

  // // get b-jets
  // auto candBJets = filterObjects(signalJets, 30., 2.5);
  // auto bjets = filterObjects(signalJets, 30., 2.5, BTag77MV2c10);

  int n_bjets       = signal_bjets.size();
  int n_jets        = signal_jets.size();
  int n_leptons     = signal_leptons.size();
  int n_base_lep    = base_leptons.size();

  // require at least 4 signal jets
  if(n_jets < 4) return;

#ifdef ROOTCORE_PACKAGE_BTaggingTruthTagging
  int seed=n_jets + 10*n_leptons + 100*met + 1000*base_electrons.size() + 10000*base_muons.size();

  std::vector<double> pt   = std::vector<double>(signal_jets.size(), 0);
  std::vector<double> eta  = std::vector<double>(signal_jets.size(), 0);
  std::vector<int>    flav = std::vector<int>   (signal_jets.size(), 1);
  std::vector<double> tagw = std::vector<double>(signal_jets.size(), 1);

  for (unsigned int index = 0; index < signal_jets.size(); ++index){
  auto jet = signal_jets.at(index);
  pt[index]   = jet.Pt()*1.e3;
  eta[index]  = jet.Eta();
  flav[index] = jetFlavor(jet);
}

  Analysis::TruthTagResults m_TTres;
  StatusCode code = m_btt->CalculateResults(pt,eta,flav,tagw,m_TTres,seed);
  if (!code.isSuccess()) throw std::runtime_error("error in retrieving the weights");
#else
  // require at least 3 b-bjets
  if(n_bjets<3) return;
#endif

  // inclusive - all jets + leptons
  float meffi  = met + sumObjectsPt(signal_jets) + sumObjectsPt(signal_leptons);
  // min mT of leading 3 bjets
  float mTbmin    = calcMTmin(signal_bjets, metVec,3);
  // dphimin between leading 4 signal jets and met
  float dphiMin4  = minDphi(metVec, signal_jets, 4);
  // leading lepton and met
  float mT = (signal_leptons.size()>0)? calcMT(signal_leptons[0], metVec) : 0.0;
  // sum of leading 4 reclustered jet masses
  float mjsum = sumObjectsM(largeR_jets, 4);
  // dPhi(j1, MET) for Gbb
  float dphi1jet = minDphi(metVec, signal_jets, 1);

  // NN lepton pT variables
  float m_pt_lep_1  = 0;
  float m_eta_lep_1 = 0;
  float m_phi_lep_1 = 0;
  float m_m_lep_1   = 0;
  float m_pt_lep_2  = 0;
  float m_eta_lep_2 = 0;
  float m_phi_lep_2 = 0;
  float m_m_lep_2   = 0;
  float m_pt_lep_3  = 0;
  float m_eta_lep_3 = 0;
  float m_phi_lep_3 = 0;
  float m_m_lep_3   = 0;
  float m_pt_lep_4  = 0;
  float m_eta_lep_4 = 0;
  float m_phi_lep_4 = 0;
  float m_m_lep_4   = 0;


  for (unsigned int index = 0; index < signal_leptons.size(); ++index){
    auto lep = signal_leptons.at(index);
    if(index==0){
      m_pt_lep_1 = lep.Pt();
      m_eta_lep_1 = lep.Eta();
      m_phi_lep_1 = lep.Phi();
      m_m_lep_1 = lep.M();
    } else if(index==1){
      m_pt_lep_2 = lep.Pt();
      m_eta_lep_2 = lep.Eta();
      m_phi_lep_2 = lep.Phi();
      m_m_lep_2 = lep.M();
    } else if(index==2){
      m_pt_lep_3 = lep.Pt();
      m_eta_lep_3 = lep.Eta();
      m_phi_lep_3 = lep.Phi();
      m_m_lep_3 = lep.M();
    } else if(index==3){
      m_pt_lep_4 = lep.Pt();
      m_eta_lep_4 = lep.Eta();
      m_phi_lep_4 = lep.Phi();
      m_m_lep_4 = lep.M();
    }
  }

  // NN smal-R jets variables
  float m_pt_jet_1  = 0;
  float m_eta_jet_1 = 0;
  float m_phi_jet_1 = 0;
  float m_m_jet_1   = 0;
  int m_isb_jet_1 = 0;
  float m_pt_jet_2  = 0;
  float m_eta_jet_2 = 0;
  float m_phi_jet_2 = 0;
  float m_m_jet_2   = 0;
  int m_isb_jet_2 = 0;
  float m_pt_jet_3  = 0;
  float m_eta_jet_3 = 0;
  float m_phi_jet_3 = 0;
  float m_m_jet_3   = 0;
  int m_isb_jet_3 = 0;
  float m_pt_jet_4  = 0;
  float m_eta_jet_4 = 0;
  float m_phi_jet_4 = 0;
  float m_m_jet_4   = 0;
  int m_isb_jet_4 = 0;
  float m_pt_jet_5  = 0;
  float m_eta_jet_5 = 0;
  float m_phi_jet_5 = 0;
  float m_m_jet_5   = 0;
  int m_isb_jet_5 = 0;
  float m_pt_jet_6  = 0;
  float m_eta_jet_6 = 0;
  float m_phi_jet_6 = 0;
  float m_m_jet_6   = 0;
  int m_isb_jet_6 = 0;
  float m_pt_jet_7  = 0;
  float m_eta_jet_7 = 0;
  float m_phi_jet_7 = 0;
  float m_m_jet_7   = 0;
  int m_isb_jet_7 = 0;
  float m_pt_jet_8  = 0;
  float m_eta_jet_8 = 0;
  float m_phi_jet_8 = 0;
  float m_m_jet_8   = 0;
  int m_isb_jet_8 = 0;
  float m_pt_jet_9  = 0;
  float m_eta_jet_9 = 0;
  float m_phi_jet_9 = 0;
  float m_m_jet_9   = 0;
  int m_isb_jet_9 = 0;
  float m_pt_jet_10  = 0;
  float m_eta_jet_10 = 0;
  float m_phi_jet_10 = 0;
  float m_m_jet_10   = 0;
  int m_isb_jet_10 = 0;

  std::vector<bool> btagged;
  for(const auto& jet: signal_jets){
    btagged.push_back(std::find(signal_bjets.begin(), signal_bjets.end(), jet) != signal_bjets.end());
  }
  for (unsigned int index = 0; index < signal_jets.size(); ++index){
    auto jet = signal_jets.at(index);
    if(index==0){
      m_pt_jet_1 = jet.Pt();
      m_eta_jet_1 = jet.Eta();
      m_phi_jet_1 = jet.Phi();
      m_m_jet_1 = jet.M();
      m_isb_jet_1 = btagged[index]?1:0;
    } else if(index==1){
      m_pt_jet_2 = jet.Pt();
      m_eta_jet_2 = jet.Eta();
      m_phi_jet_2 = jet.Phi();
      m_m_jet_2 = jet.M();
      m_isb_jet_2 = btagged[index]?1:0;
    } else if(index==2){
      m_pt_jet_3 = jet.Pt();
      m_eta_jet_3 = jet.Eta();
      m_phi_jet_3 = jet.Phi();
      m_m_jet_3 = jet.M();
      m_isb_jet_3 = btagged[index]?1:0;
    } else if(index==3){
      m_pt_jet_4 = jet.Pt();
      m_eta_jet_4 = jet.Eta();
      m_phi_jet_4 = jet.Phi();
      m_m_jet_4 = jet.M();
      m_isb_jet_4 = btagged[index]?1:0;
    } else if(index==4){
      m_pt_jet_5 = jet.Pt();
      m_eta_jet_5 = jet.Eta();
      m_phi_jet_5 = jet.Phi();
      m_m_jet_5 = jet.M();
      m_isb_jet_5 = btagged[index]?1:0;
    } else if(index==5){
      m_pt_jet_6 = jet.Pt();
      m_eta_jet_6 = jet.Eta();
      m_phi_jet_6 = jet.Phi();
      m_m_jet_6 = jet.M();
      m_isb_jet_6 = btagged[index]?1:0;
    } else if(index==6){
      m_pt_jet_7 = jet.Pt();
      m_eta_jet_7 = jet.Eta();
      m_phi_jet_7 = jet.Phi();
      m_m_jet_7 = jet.M();
      m_isb_jet_7 = btagged[index]?1:0;
    } else if(index==7){
      m_pt_jet_8 = jet.Pt();
      m_eta_jet_8 = jet.Eta();
      m_phi_jet_8 = jet.Phi();
      m_m_jet_8 = jet.M();
      m_isb_jet_8 = btagged[index]?1:0;
    } else if(index==8){
      m_pt_jet_9 = jet.Pt();
      m_eta_jet_9 = jet.Eta();
      m_phi_jet_9 = jet.Phi();
      m_m_jet_9 = jet.M();
      m_isb_jet_9 = btagged[index]?1:0;
    } else if(index==9){
      m_pt_jet_10 = jet.Pt();
      m_eta_jet_10 = jet.Eta();
      m_phi_jet_10 = jet.Phi();
      m_m_jet_10 = jet.M();
      m_isb_jet_10 = btagged[index]?1:0;
    }
  }

  // NN large-R jets variables
  float l_pt_jet_1  = 0;
  float l_eta_jet_1 = 0;
  float l_phi_jet_1 = 0;
  float l_m_jet_1   = 0;
  float l_pt_jet_2  = 0;
  float l_eta_jet_2 = 0;
  float l_phi_jet_2 = 0;
  float l_m_jet_2   = 0;
  float l_pt_jet_3  = 0;
  float l_eta_jet_3 = 0;
  float l_phi_jet_3 = 0;
  float l_m_jet_3   = 0;
  float l_pt_jet_4  = 0;
  float l_eta_jet_4 = 0;
  float l_phi_jet_4 = 0;
  float l_m_jet_4   = 0;

  for (unsigned int index = 0; index < largeR_jets.size(); ++index){
    auto jet = largeR_jets.at(index);
    if(index==0){
      l_pt_jet_1 = jet.Pt();
      l_eta_jet_1 = jet.Eta();
      l_phi_jet_1 = jet.Phi();
      l_m_jet_1 = jet.M();
    } else if(index==1){
      l_pt_jet_2 = jet.Pt();
      l_eta_jet_2 = jet.Eta();
      l_phi_jet_2 = jet.Phi();
      l_m_jet_2 = jet.M();
    } else if(index==2){
      l_pt_jet_3 = jet.Pt();
      l_eta_jet_3 = jet.Eta();
      l_phi_jet_3 = jet.Phi();
      l_m_jet_3 = jet.M();
    } else if(index==3){
      l_pt_jet_4 = jet.Pt();
      l_eta_jet_4 = jet.Eta();
      l_phi_jet_4 = jet.Phi();
      l_m_jet_4 = jet.M();
    }
  }

  fill("meffi", meffi);
  fill("met", met);
  fill("mjsum", mjsum);
  fill("mTb_min", mTbmin);
  fill("mT", mT);
  fill("dphiMin4", dphiMin4);
  fill("dphi1jet", dphi1jet);

  fill("n_jets", n_jets);
  fill("n_bjets", n_bjets);
  fill("n_electrons", signal_electrons.size());
  fill("n_muons", signal_muons.size());
  fill("n_leptons", n_leptons);

  // process through NN
  auto nn = getMVA("model");
  nn_inputs = {
    // small-R jets
    static_cast<float>(m_pt_jet_1), static_cast<float>(m_eta_jet_1), static_cast<float>(m_phi_jet_1), static_cast<float>(m_m_jet_1), static_cast<float>(m_isb_jet_1),
    static_cast<float>(m_pt_jet_2), static_cast<float>(m_eta_jet_2), static_cast<float>(m_phi_jet_2), static_cast<float>(m_m_jet_2), static_cast<float>(m_isb_jet_2),
    static_cast<float>(m_pt_jet_3), static_cast<float>(m_eta_jet_3), static_cast<float>(m_phi_jet_3), static_cast<float>(m_m_jet_3), static_cast<float>(m_isb_jet_3),
    static_cast<float>(m_pt_jet_4), static_cast<float>(m_eta_jet_4), static_cast<float>(m_phi_jet_4), static_cast<float>(m_m_jet_4), static_cast<float>(m_isb_jet_4),
    static_cast<float>(m_pt_jet_5), static_cast<float>(m_eta_jet_5), static_cast<float>(m_phi_jet_5), static_cast<float>(m_m_jet_5), static_cast<float>(m_isb_jet_5),
    static_cast<float>(m_pt_jet_6), static_cast<float>(m_eta_jet_6), static_cast<float>(m_phi_jet_6), static_cast<float>(m_m_jet_6), static_cast<float>(m_isb_jet_6),
    static_cast<float>(m_pt_jet_7), static_cast<float>(m_eta_jet_7), static_cast<float>(m_phi_jet_7), static_cast<float>(m_m_jet_7), static_cast<float>(m_isb_jet_7),
    static_cast<float>(m_pt_jet_8), static_cast<float>(m_eta_jet_8), static_cast<float>(m_phi_jet_8), static_cast<float>(m_m_jet_8), static_cast<float>(m_isb_jet_8),
    static_cast<float>(m_pt_jet_9), static_cast<float>(m_eta_jet_9), static_cast<float>(m_phi_jet_9), static_cast<float>(m_m_jet_9), static_cast<float>(m_isb_jet_9),
    static_cast<float>(m_pt_jet_10), static_cast<float>(m_eta_jet_10), static_cast<float>(m_phi_jet_10), static_cast<float>(m_m_jet_10), static_cast<float>(m_isb_jet_10),
    // large-R jets
    static_cast<float>(l_pt_jet_1), static_cast<float>(l_eta_jet_1), static_cast<float>(l_phi_jet_1), static_cast<float>(l_m_jet_1),
    static_cast<float>(l_pt_jet_2), static_cast<float>(l_eta_jet_2), static_cast<float>(l_phi_jet_2), static_cast<float>(l_m_jet_2),
    static_cast<float>(l_pt_jet_3), static_cast<float>(l_eta_jet_3), static_cast<float>(l_phi_jet_3), static_cast<float>(l_m_jet_3),
    static_cast<float>(l_pt_jet_4), static_cast<float>(l_eta_jet_4), static_cast<float>(l_phi_jet_4), static_cast<float>(l_m_jet_4),
    // leptons
    static_cast<float>(m_pt_lep_1), static_cast<float>(m_eta_lep_1), static_cast<float>(m_phi_lep_1), static_cast<float>(m_m_lep_1),
    static_cast<float>(m_pt_lep_2), static_cast<float>(m_eta_lep_2), static_cast<float>(m_phi_lep_2), static_cast<float>(m_m_lep_2),
    static_cast<float>(m_pt_lep_3), static_cast<float>(m_eta_lep_3), static_cast<float>(m_phi_lep_3), static_cast<float>(m_m_lep_3),
    static_cast<float>(m_pt_lep_4), static_cast<float>(m_eta_lep_4), static_cast<float>(m_phi_lep_4), static_cast<float>(m_m_lep_4),
    // met
    static_cast<float>(met), static_cast<float>(met_phi),
    // parameters
    0.0, 0.0, 0.0
  };

  // apply normalization (for all but the 3 parameters at the end)
  for(size_t i=0; i < nn_inputs.size()-3; i++){
    nn_inputs[i] = (nn_inputs[i] - NN_config.normalization.first[i])/NN_config.normalization.second[i];
  }

  std::map<std::string,std::vector<float>> NNscores;
  // go through each parameter and evaluate the NN
  NNscores.clear();
  for(const auto& parameter: NN_config.parameters){
    nn_inputs[84] = (static_cast<float>(parameter.isGtt) - NN_config.normalization.first[84]) / NN_config.normalization.second[84];
    nn_inputs[85] = (static_cast<float>(parameter.mGluino) - NN_config.normalization.first[85]) / NN_config.normalization.second[85];
    nn_inputs[86] = (static_cast<float>(parameter.mLSP) - NN_config.normalization.first[86]) / NN_config.normalization.second[86];

    const std::vector<float> nn_value = nn->evaluateMulti(nn_inputs, 8);
    ntupVar(parameter.stringify(), nn_value);
    NNscores[parameter.stringify()]=std::vector<float>(nn_value);

  }





  if(n_leptons == 0 && dphiMin4 > 0.4){
    // handle Gbb SR
    if(n_jets >=4   && mTbmin > 130 &&               n_bjets >= 3 &&                met > 550 && meffi > 2600   && m_pt_jet_1 >65  ) accept("SR_Gbb_B");
    if(n_jets >=4   && mTbmin > 130 &&               n_bjets >= 3 &&                met > 550 && meffi > 2000   && m_pt_jet_1>30            ) accept("SR_Gbb_M");
    if(n_jets >=4   && mTbmin > 130 &&               n_bjets >= 3 &&                met > 550 && meffi > 1600   && m_pt_jet_1>30           ) accept("SR_Gbb_C");
  }


  if(n_leptons == 1 &&                n_jets >= 4){
      // handle Gbb CR
      if(                n_bjets >= 3 &&                met > 550 && meffi > 2600 && mT<200      && m_pt_jet_1>65         ) accept("CR_Gbb_B");
      if(                n_bjets >= 4 &&                met > 400 && meffi > 2000 && mT<250        && m_pt_jet_1>30      ) accept("CR_Gbb_M");
      if(                n_bjets >= 4 &&                met > 450 && meffi > 1600 && mT<200         && m_pt_jet_1>30                      ) accept("CR_Gbb_C");
  }

  if(n_leptons == 0 && dphiMin4 > 0.4){
    // handle Gbb VR
      if(n_bjets>=3 && n_jets >=4 && met > 550&& meffi < 2400 && mTbmin >= 130 && m_pt_jet_1>65) accept("VR_Gbb_B");
      if(mTbmin >=  80 && n_bjets>=3 && n_jets >=4 && met < 500 && meffi > 1600 && m_pt_jet_1>30               ) accept("VR_Gbb_M");
      if(mTbmin >= 130 && n_bjets>=3 && n_jets >=4 && met < 450 && meffi > 1500 && m_pt_jet_1>30              ) accept("VR_Gbb_C");
  }

  if(n_leptons == 0 && dphiMin4 > 0.4){
    // handle Gtb SR
    if(n_jets >=4   && mTbmin > 130 &&               n_bjets >= 3 &&                met > 550 && meffi > 2500 && mjsum>=200) accept("SR_Gtb_B");
    if(n_jets >=6   && mTbmin > 130 &&               n_bjets >= 4 &&                met > 550 && meffi > 2000 && mjsum>=200) accept("SR_Gtb_M");
    if(n_jets >=7   && mTbmin > 130 &&               n_bjets >= 4 &&                met > 500 && meffi > 1300 && mjsum>=50) accept("SR_Gtb_C");
  }

  if(n_leptons == 1){
    // handle Gtb CR
    if(n_jets >=4 &&         n_bjets >= 3 &&            met > 400 && meffi > 2200 && mT<80 && mjsum>=200) accept("CR_Gtb_B");
    if(n_bjets >= 6 &&       n_bjets >= 4 &&met > 350 && meffi > 1800 && mT<200 &&        mjsum>=200) accept("CR_Gtb_M");
    if(n_bjets >= 7 &&       n_bjets >= 4 &&met > 350 && meffi > 1300 && mT<200 &&        mjsum>=50) accept("CR_Gtb_C");
  }

  if(n_leptons == 0 && dphiMin4 > 0.4){
    // handle Gtb VR
    if(mTbmin >= 130 && n_bjets>=3 && n_jets >=4 && met > 450 && meffi < 2500 && mjsum>=200) accept("VR_Gtb_B");
    if(mTbmin >= 100 && n_bjets>=4 && n_jets >=6 && met < 550 && meffi > 1600 && mjsum>=200) accept("VR_Gtb_M");
    if(mTbmin >= 80 && n_bjets>=4 && n_jets >=7 && met < 500 && meffi > 1300 && mjsum>=50) accept("VR_Gtb_C");
  }

  if(n_leptons == 0 && dphiMin4 > 0.4){
    // handle Gtt 0L SR
    if(mTbmin > 120  && n_bjets >= 3 && n_jets >= 5 && met > 600 && meffi > 2900 && mjsum > 300) accept("SR_Gtt_0L_B");
    if(mTbmin > 120 && n_bjets >= 3 && n_jets >= 9 && met > 600 && meffi > 1700 && mjsum > 300) accept("SR_Gtt_0L_M1");
    if(mTbmin > 120 && n_bjets >= 3 && n_jets >= 9 && met > 500 && meffi > 1100 && mjsum > 200) accept("SR_Gtt_0L_M2");
    if(mTbmin > 180 && n_bjets >= 4 && n_jets >= 10 && met > 400 && meffi > 800 && mjsum > 100) accept("SR_Gtt_0L_C");
  }

  if(n_leptons == 1){
    // handle Gtt 0L CR
    if(n_bjets >= 3 && n_jets >= 4 && met > 200 && meffi > 2000 && mjsum > 150) accept("CR_Gtt_0L_B");
    if(n_bjets >= 3 && n_jets >= 8 && met > 200 && meffi > 1100 && mjsum > 150) accept("CR_Gtt_0L_M1");
    if(n_bjets >= 3 && n_jets >= 8 && met > 200 && meffi > 800 && mjsum > 100) accept("CR_Gtt_0L_M2");
    if(n_bjets >= 4 && n_jets >= 9 && met > 200 && meffi > 800 && mjsum > 100) accept("CR_Gtt_0L_C");
  }

  if(n_leptons == 0 && dphiMin4 > 0.4){
    // handle Gtt 0L VR
    if(n_bjets >= 3 && n_jets >= 5 && met > 250 && meffi > 2000 && mjsum <= 300) accept("VR_Gtt_0L_B");
    if(n_bjets >= 3 && n_jets >= 9 && met > 300 && meffi > 1100 && mjsum <= 300) accept("VR_Gtt_0L_M1");
    if(n_bjets >= 3 && n_jets >= 10 && met > 300 && meffi > 800 && mjsum <= 200) accept("VR_Gtt_0L_M2");
    if(n_bjets >= 4 && n_jets >= 10 && met > 200 && meffi > 800 && mjsum <= 100) accept("VR_Gtt_0L_C");
  }

  if(n_leptons >= 1){
    // handle Gtt 1L SR
    if(mTbmin > 120 && n_bjets >= 3 && n_jets >= 4 && met > 600 && meffi > 2300 && mjsum > 200) accept("SR_Gtt_1L_B");
    if(mTbmin > 120 && n_bjets >= 3 && n_jets >= 5 && met > 600 && meffi > 2000 && mjsum > 200) accept("SR_Gtt_1L_M1");
    if(mTbmin > 120 && n_bjets >= 3 && n_jets >= 8 && met > 500 && meffi > 1100 && mjsum > 100) accept("SR_Gtt_1L_M2");
    if(mTbmin > 180 && n_bjets >= 3 && n_jets >= 9 && met > 300 && meffi > 800 && mjsum > 30) accept("SR_Gtt_1L_C");
  }

  if(n_leptons >= 1){
    // handle Gtt 1L CR
    if(n_bjets >= 3 && n_jets == 4 && met > 200 && meffi > 1500 && mT<=150) accept("CR_Gtt_1L_B");
    if(n_bjets >= 3 && n_jets == 5 && met > 200 && meffi > 1200 && mT<=200) accept("CR_Gtt_1L_M1");
    if(n_bjets >= 3 && n_jets == 8 && met > 200 && meffi > 800 && mT<=200) accept("CR_Gtt_1L_M2");
    if(n_bjets >= 3 && n_jets == 9 && met > 200 && meffi > 800 && mT<=150) accept("CR_Gtt_1L_C");
  }

  if(n_leptons >= 1){
    // handle Gtt 1L VR1
    if(n_bjets >= 3 && n_jets > 4 && met > 200 && meffi > 1500 && mT<=150) accept("VR1_Gtt_1L_B");
    if(n_bjets >= 3 && n_jets > 5 && met > 200 && meffi > 1200 && mT<=200) accept("VR1_Gtt_1L_M1");
    if(n_bjets >= 3 && n_jets > 8 && met > 200 && meffi > 800 && mT<=200) accept("VR1_Gtt_1L_M2");
    if(n_bjets >= 3 && n_jets > 9 && met > 200 && meffi > 800 && mT<=150) accept("VR1_Gtt_1L_C");
  }

  if(n_leptons >= 1){
    // handle Gtt 1L VR2
    if(n_bjets >= 3 && n_jets > 4 && met > 200 && meffi > 1200 && mT<=150 && mTbmin > 120 && mjsum > 200  ) accept("VR2_Gtt_1L_B");
    if(n_bjets >= 3 && n_jets > 5 && met > 200 && meffi > 1000 && mT<=200 && mTbmin > 120 && mjsum > 100) accept("VR2_Gtt_1L_M1");
    if(n_bjets >= 3 && n_jets > 8 && met > 200 && meffi > 800 && mT<=200 && mTbmin > 120 && mjsum > 100) accept("VR2_Gtt_1L_M2");
    if(n_bjets >= 3 && n_jets > 9 && met > 200 && meffi > 800 && mT<=150 && mTbmin > 120) accept("VR2_Gtt_1L_C");
  }

  if(n_leptons==0 &&  dphiMin4>0.4 && met>200 && n_jets>=4 && n_bjets>=3){

      if(NNscores["NNoutput_0_2800_1400"][1]>=0.999 && dphiMin4>=0.6) accept("SR_Gbb_2800_1400");
      if(NNscores["NNoutput_0_2300_1000"][1]>=0.9994 && dphiMin4>=0.6) accept("SR_Gbb_2300_1000");
      if(NNscores["NNoutput_0_2100_1600"][1]>=0.9993 && dphiMin4>=0.4) accept("SR_Gbb_2100_1600");
      if(NNscores["NNoutput_0_2000_1800"][1]>=0.997 && dphiMin4>=0.4) accept("SR_Gbb_2000_1800");
  }
  if(((n_leptons==0 && dphiMin4>0.4) || (n_leptons>=1)) && met>200 && n_jets>=4 && n_bjets>=3){


      if(NNscores["NNoutput_1_2100_1"][0]>=0.9997){
	  accept("SR_Gtt_2100_1");
      }
      if(NNscores["NNoutput_1_1800_1"][0]>=0.9997){
	  accept("SR_Gtt_1800_1");
      }

      if(NNscores["NNoutput_1_2300_1200"][0]>=0.9993) {
	  accept("SR_Gtt_2300_1200");
      }
      if(NNscores["NNoutput_1_1900_1400"][0]>=0.9987){
	  accept("SR_Gtt_1900_1400");
      }

  }

  ntupVar("mc_weight", event->getMCWeights()[0]);

  ntupVar("gen_met", gen_met);
  ntupVar("gen_ht", gen_ht);
  ntupVar("channel_number", channel_number);

  ntupVar("meff_incl", meffi);
  ntupVar("met", met);
  ntupVar("met_phi", met_phi);
  ntupVar("MJSum_rc_r08pt10", mjsum);
  ntupVar("mTb_min", mTbmin);
  ntupVar("mT", mT);
  ntupVar("dphi_min", dphiMin4);
  ntupVar("dphi_min_1", dphi1jet);

  ntupVar("jets_n", n_jets);
  ntupVar("bjets_n", n_bjets);
  ntupVar("baseline_electrons_n", static_cast<int>(base_electrons.size()));
  ntupVar("signal_electrons_n", static_cast<int>(signal_electrons.size()));
  ntupVar("baseline_muons_n", static_cast<int>(base_muons.size()));
  ntupVar("signal_muons_n", static_cast<int>(signal_muons.size()));
  ntupVar("signal_leptons_n", n_leptons);
  ntupVar("baseline_leptons_n", n_base_lep);

  ntupVar("signalJets0_pass_BTag77MV2c10", signal_jets[0].pass(BTag77MV2c10));
  ntupVar("signalJets0_Pt", signal_jets[0].Pt());
  ntupVar("signalJets3_Pt", signal_jets[3].Pt());

  ntupVar("truth_id0", jetFlavor(signal_jets[0]));
  ntupVar("truth_id1", jetFlavor(signal_jets[1]));
  ntupVar("truth_id2", jetFlavor(signal_jets[2]));
  ntupVar("truth_id3", jetFlavor(signal_jets[3]));

  // NN branches
  ntupVar("leptons_pt_0", m_pt_lep_1);
  ntupVar("leptons_eta_0", m_eta_lep_1);
  ntupVar("leptons_phi_0", m_phi_lep_1);
  ntupVar("leptons_m_0", m_m_lep_1);
  ntupVar("leptons_pt_1", m_pt_lep_2);
  ntupVar("leptons_eta_1", m_eta_lep_2);
  ntupVar("leptons_phi_1", m_phi_lep_2);
  ntupVar("leptons_m_1", m_m_lep_2);
  ntupVar("leptons_pt_2", m_pt_lep_3);
  ntupVar("leptons_eta_2", m_eta_lep_3);
  ntupVar("leptons_phi_2", m_phi_lep_3);
  ntupVar("leptons_m_2", m_m_lep_3);
  ntupVar("leptons_pt_3", m_pt_lep_4);
  ntupVar("leptons_eta_3", m_eta_lep_4);
  ntupVar("leptons_phi_3", m_phi_lep_4);
  ntupVar("leptons_m_3", m_m_lep_4);
  ntupVar("small_R_jets_pt_0", m_pt_jet_1);
  ntupVar("small_R_jets_eta_0", m_eta_jet_1);
  ntupVar("small_R_jets_phi_0", m_phi_jet_1);
  ntupVar("small_R_jets_m_0", m_m_jet_1);
  ntupVar("small_R_jets_isb_0", m_isb_jet_1);
  ntupVar("small_R_jets_pt_1", m_pt_jet_2);
  ntupVar("small_R_jets_eta_1", m_eta_jet_2);
  ntupVar("small_R_jets_phi_1", m_phi_jet_2);
  ntupVar("small_R_jets_m_1", m_m_jet_2);
  ntupVar("small_R_jets_isb_1", m_isb_jet_2);
  ntupVar("small_R_jets_pt_2", m_pt_jet_3);
  ntupVar("small_R_jets_eta_2", m_eta_jet_3);
  ntupVar("small_R_jets_phi_2", m_phi_jet_3);
  ntupVar("small_R_jets_m_2", m_m_jet_3);
  ntupVar("small_R_jets_isb_2", m_isb_jet_3);
  ntupVar("small_R_jets_pt_3", m_pt_jet_4);
  ntupVar("small_R_jets_eta_3", m_eta_jet_4);
  ntupVar("small_R_jets_phi_3", m_phi_jet_4);
  ntupVar("small_R_jets_m_3", m_m_jet_4);
  ntupVar("small_R_jets_isb_3", m_isb_jet_4);
  ntupVar("small_R_jets_pt_4", m_pt_jet_5);
  ntupVar("small_R_jets_eta_4", m_eta_jet_5);
  ntupVar("small_R_jets_phi_4", m_phi_jet_5);
  ntupVar("small_R_jets_m_4", m_m_jet_5);
  ntupVar("small_R_jets_isb_4", m_isb_jet_5);
  ntupVar("small_R_jets_pt_5", m_pt_jet_6);
  ntupVar("small_R_jets_eta_5", m_eta_jet_6);
  ntupVar("small_R_jets_phi_5", m_phi_jet_6);
  ntupVar("small_R_jets_m_5", m_m_jet_6);
  ntupVar("small_R_jets_isb_5", m_isb_jet_6);
  ntupVar("small_R_jets_pt_6", m_pt_jet_7);
  ntupVar("small_R_jets_eta_6", m_eta_jet_7);
  ntupVar("small_R_jets_phi_6", m_phi_jet_7);
  ntupVar("small_R_jets_m_6", m_m_jet_7);
  ntupVar("small_R_jets_isb_6", m_isb_jet_7);
  ntupVar("small_R_jets_pt_7", m_pt_jet_8);
  ntupVar("small_R_jets_eta_7", m_eta_jet_8);
  ntupVar("small_R_jets_phi_7", m_phi_jet_8);
  ntupVar("small_R_jets_m_7", m_m_jet_8);
  ntupVar("small_R_jets_isb_7", m_isb_jet_8);
  ntupVar("small_R_jets_pt_8", m_pt_jet_9);
  ntupVar("small_R_jets_eta_8", m_eta_jet_9);
  ntupVar("small_R_jets_phi_8", m_phi_jet_9);
  ntupVar("small_R_jets_m_8", m_m_jet_9);
  ntupVar("small_R_jets_isb_8", m_isb_jet_9);
  ntupVar("small_R_jets_pt_9", m_pt_jet_10);
  ntupVar("small_R_jets_eta_9", m_eta_jet_10);
  ntupVar("small_R_jets_phi_9", m_phi_jet_10);
  ntupVar("small_R_jets_m_9", m_m_jet_10);
  ntupVar("small_R_jets_isb_9", m_isb_jet_10);

  ntupVar("large_R_jets_pt_0", l_pt_jet_1);
  ntupVar("large_R_jets_eta_0", l_eta_jet_1);
  ntupVar("large_R_jets_phi_0", l_phi_jet_1);
  ntupVar("large_R_jets_m_0", l_m_jet_1);
  ntupVar("large_R_jets_pt_1", l_pt_jet_2);
  ntupVar("large_R_jets_eta_1", l_eta_jet_2);
  ntupVar("large_R_jets_phi_1", l_phi_jet_2);
  ntupVar("large_R_jets_m_1", l_m_jet_2);
  ntupVar("large_R_jets_pt_2", l_pt_jet_3);
  ntupVar("large_R_jets_eta_2", l_eta_jet_3);
  ntupVar("large_R_jets_phi_2", l_phi_jet_3);
  ntupVar("large_R_jets_m_2", l_m_jet_3);
  ntupVar("large_R_jets_pt_3", l_pt_jet_4);
  ntupVar("large_R_jets_eta_3", l_eta_jet_4);
  ntupVar("large_R_jets_phi_3", l_phi_jet_4);
  ntupVar("large_R_jets_m_3", l_m_jet_4);


#ifdef ROOTCORE_PACKAGE_BTaggingTruthTagging
  ntupVar("weight_3b_in", m_TTres.map_trf_weight_in["Nominal"].at(3));
  ntupVar("weight_4b_in", m_TTres.map_trf_weight_in["Nominal"].at(4));
#endif

  return;
}
